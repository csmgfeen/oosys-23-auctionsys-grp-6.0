package OOSyScw1;

import java.time.LocalDateTime;

public class Item {
	
	//creating the variables
	private int no;
	private String item;
	private float sprice;
	private float rprice;
	//private LocalDateTime closedt;
	private boolean status;
	private float topbid;
	
	//setting the variables
	public Item(int no, String item, float sprice, float rprice, boolean status, float topbid) {
		this.no = no;
		this.item = item;
		this.sprice = sprice;
		this.rprice = rprice;
		//this.closedt = closedt;
		this.status = status;
		this.topbid = topbid;
	}

}
