package OOSyScw1;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.io.PrintWriter;
import java.io.File;
import java.time.LocalDateTime;
import java.io.BufferedReader;

public class designDev {
		//Implementing the scanner
		private static Scanner input = new Scanner(System.in);
		
		private static final Item[] items = new Item[0];

	public static void main(String[] args) throws IOException{
		
		String choice = "";
		
		do {
			System.out.println("-- eAuction --");
			System.out.println("\n--MAIN MENU--");
			System.out.println("1 - Browse/Search");
			System.out.println("2 - Register");
			System.out.println("3 - Admin");
			System.out.println("Q - Quit");
			System.out.print("Pick: ");
			
			//stores uses input as a string 
			choice = input.next().toUpperCase();
			
			//switch case to run method to whatever user enters 
			switch (choice) {
			case "1" : {
				browseList();
				break;
			}
			case "2" : {
				createAccount();
				bidsellMenu();
				break;
			}
			case "3" : {
				blockUser();
				break;
			}
			}
						
			}while (!choice.equals("Q"));
		System.out.println("--GOODBYE--");

	}
	
	private static void browseList() throws IOException {
		
		/*for (int i = 0; i < items.length; i++)
			System.out.println(items[i].toString());*/
		
		System.out.println("Do you want to Bid/sell? (Y/N)");
		String yn = input.next().toUpperCase();
		
		switch (yn) {
		case "Y" : {
			createAccount();
			bidsellMenu();
			break;
		}
		case "N" : {
			System.out.println("--GOODBYE--");
			System.exit(0);
			break;
		}
		}
		
		/*if (yn == "Y") {
			createAccount();
			bidsellMenu();
		}
		else if (yn == "N"){
			System.out.println("--GOODBYE--");
			System.exit(0);
		}*/
		
	}
	
	private static void createAccount() throws IOException {
		
		System.out.print("\nCreate a Username: "); //Users will be prompted to register an account.
		String createUser = input.next();
		
		System.out.print("Enter a Password: ");
		String createPass = input.next();
		
		try{
			File logon = new File("/Users/glenfeeney/Desktop/logon.txt"); //User's credentials will be stored in a text file so they are kept on the system.
			
			FileWriter fw = new FileWriter(logon, true);
			PrintWriter pw = new PrintWriter(fw);
			pw.println(createUser + " " + createPass);
			pw.close();
	    	}catch (FileNotFoundException e) {
	    	e.printStackTrace();
	    	}
	}
	
	private static void bidsellMenu() {
		
		String choice = "";
		
		do {
			System.out.println("\n-- MENU --");
			System.out.println("1 - Bid");
			System.out.println("2 - Sell");
			System.out.println("X - LogOff");
			System.out.print("Pick: ");
			
			choice = input.next().toUpperCase();
			
			switch (choice) {
			case "1" : {
				bidItem();
				break;
			}
			case "2" : {
				sellItem();
				break;
			}
			}
						
			}while (!choice.equals("X"));
		System.out.println("--GOODBYE--");
		
	}
	
	private static void bidItem() {
		
		
		
		System.out.print("\nPick an item to bid on: ");
		String itemNo = input.next();
		System.out.print("Enter your bid price: £");
		String bidPrice = input.next();
		
	}

	private static void sellItem() {
		
	}
	
	private static void blockUser() {
		
		System.out.print("Enter username to be blocked: "); //System temporarily blocks the user if theya re already registered.
		String blockuser = input.next();
		
		
		File logon = new File("/Users/glenfeeney/Desktop/logon.txt");//change to your own
		
		// read through file 
		try (BufferedReader reader = new BufferedReader(new FileReader(logon));
	             FileWriter writer = new FileWriter(logon)) {
			
				
	            String line = reader.readLine();

	            //while loop to compare users input to file 
	            while (line != null) {
	                if (!line.trim().equals(blockuser)) {
	                    writer.write(line + System.lineSeparator());
	                }
	                line = reader.readLine();
	            }

	            System.out.println("Line removed from file.");
	        } catch (IOException e) {
	            System.out.println("Error while processing the file: " + e.getMessage());
	        }
		
	}
	
	private static void loadItems() throws FileNotFoundException{
		Scanner file = new Scanner(new FileReader("/Users/glenfeeney/Desktop/auction.txt"));
		int index = 0;
		
		//creating while loop to read through the file and save it in array
		while (file.hasNext()) {
			int no = Integer.parseInt(file.next());
			String item = file.next();
			float sprice = Float.parseFloat(file.next());
			float rprice = Float.parseFloat(file.next());		
			//LocalDateTime closed = LocalDateTime.parseLocalDateTime(file.next());
			boolean status = Boolean.parseBoolean(file.next());
			float topbid = Float.parseFloat(file.next());
			
			items[index] = new Item(no, item, sprice, rprice, status, topbid);
			index++;
		}
		
		file.close();
	}

}
